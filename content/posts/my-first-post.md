---
title: Happy New Year!
description: New year greetings
date: 2021-01-01T00:30:12+09:00
categories:
  - "New Year"
mathjax: true # Enable MathJax for specific page
tags:
  - "Greetings"
  - "New Year"
---

2020 has passed to quickly due to the covid19 pandemic. Hoping 2021 wouldn't be like 2020, ...
<!--more-->

# 2021  
Happy new year! Last year went by too quickly due to the pandemic and people say they have been robbed their 2020. I hope that 2021 will be different from 2020 and will plan many things so that I can be prepared.

## Math  
In the begining of this year, I will mainly focus on the [stacks project](https://stacks.math.columbia.edu/) in order to enter the field of algebraic geometry. Next I will try to tackle the work of Peter scholze and Jacob Lurie which is too long to list here... If you're curious, see my upcoming posts!

## Physics  
I am planning to study string theory as soon as possible so I will start out with Quantum field theory which I haven't yet mastered enough.

## Computer Science  
If I have time...

Update: I found out that I don't have the ability to type every math equation I want to using mathjax so I'll just link pdfs in the upcoming posts with a short summary.
